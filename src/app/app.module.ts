import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { PersonsComponent } from './persons/persons.component';
import { IndexComponentPersons } from './persons/index/index.component';
import { PersonComponent } from './persons/person/person.component';
import { DetailsComponent } from './details/details.component';
import { IndexComponentDetails } from './details/index/index.component';
import { DetailComponent } from './details/detail/detail.component';


@NgModule({
  declarations: [
    AppComponent,
    PersonsComponent,
    PersonComponent,
    DetailsComponent,
    DetailComponent,
    IndexComponentPersons,
    IndexComponentDetails
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
