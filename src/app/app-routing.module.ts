import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { PersonsComponent } from './persons/persons.component';
import { IndexComponentPersons } from './persons/index/index.component';
import { PersonComponent } from './persons/person/person.component';
import { DetailsComponent } from './details/details.component';
import { IndexComponentDetails } from './details/index/index.component';
import { DetailComponent } from './details/detail/detail.component';

const routes: Routes = [
  { path: '', redirectTo: 'person', pathMatch: 'full' },
  { path: 'person', component: PersonsComponent },
  { path: 'detail', component: DetailsComponent },
  { path: '**', component: PersonsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
